package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given

        // when

        // then

    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(ExpenseType.EXPENSE_TYPE_A, expenseCodeByProject);

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(ExpenseType.EXPENSE_TYPE_B, expenseCodeByProject);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Other Project");
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);
        // then
        Assertions.assertEquals(ExpenseType.OTHER_EXPENSE, expenseCodeByProject);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project project = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "Invalid Project");
        // when
        ExpenseService expenseService = new ExpenseService();
        // then
        Assertions.assertThrows(UnexpectedProjectTypeException.class, () -> expenseService.getExpenseCodeByProject(project));
    }
}